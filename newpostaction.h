#ifndef NEWPOSTACTION_H
#define NEWPOSTACTION_H

#include "action.h"

class XmlRpcPendingCall;

class NewPostAction : public Action
{
	Q_OBJECT
public:
	explicit NewPostAction(int topicId, const QString& text, Board *board);

	bool isSupersetOf(Action *action) const;

	void execute();

private slots:
	void handleFinishedCall();

private:
	XmlRpcPendingCall *_call;
	int _topicId;
	QString _text;
};

#endif // NEWPOSTACTION_H
