#ifndef ACTION_H
#define ACTION_H

#include <QObject>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>

class Board;

class Action : public QObject
{
	Q_OBJECT
public:
	explicit Action(Board *board);

	virtual bool isSupersetOf(Action *action) const = 0;

signals:
	void finished(Action *self);
	void error(Action *self, const QString& messsage);

public slots:
	virtual void execute() = 0;

protected:
	void handleDatabaseError(const char *what, const QSqlError& err);
	void handleDatabaseError(const char *what, const QSqlQuery& query);

protected:
	Board *_board;

};

#endif // ACTION_H
