#ifndef LOGINACTION_H
#define LOGINACTION_H

#include "action.h"

class XmlRpcPendingCall;

class LoginAction : public Action
{
	Q_OBJECT
public:
	explicit LoginAction(const QString& name, const QString& password, Board *board);

	bool isSupersetOf(Action *action) const;

	void execute();

private slots:
	void handleFinishedCall();

private:
	XmlRpcPendingCall *_call;
	QString _username;
	QString _password;
};


#endif // LOGINACTION_H
