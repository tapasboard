#ifndef FETCHFORUMSACTION_H
#define FETCHFORUMSACTION_H

#include <QtCore/QVariant>
#include "action.h"

class XmlRpcPendingCall;

class FetchForumsAction : public Action
{
	Q_OBJECT
public:
	explicit FetchForumsAction(Board *board);

	bool isSupersetOf(Action *action) const;

	void execute();

private slots:
	void handleFinishedCall();

private:
	static QList<QVariantMap> flattenForumList(const QVariantList& list, int *order);
	static QString decodeForumText(const QVariant& v);


private:
	XmlRpcPendingCall *_call;
};

#endif // FETCHFORUMSACTION_H
