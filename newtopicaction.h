#ifndef NEWTOPICACTION_H
#define NEWTOPICACTION_H

#include "action.h"

class XmlRpcPendingCall;

class NewTopicAction : public Action
{
	Q_OBJECT
public:
	explicit NewTopicAction(int forumId, const QString& subject, const QString& text, Board *board);

	bool isSupersetOf(Action *action) const;

	void execute();

private slots:
	void handleFinishedCall();

private:
	XmlRpcPendingCall *_call;
	int _forumId;
	QString _subject;
	QString _text;
};

#endif // NEWTOPICACTION_H
