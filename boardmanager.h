#ifndef BOARDMANAGER_H
#define BOARDMANAGER_H

#include <QtCore/QObject>
#include <QtCore/QHash>
#include <QtCore/QUrl>

class Board;

class BoardManager : public QObject
{
	Q_OBJECT
public:
	explicit BoardManager(QObject *parent = 0);
	
	Q_INVOKABLE Board *getBoard(const QUrl& url, const QString& username = QString(), const QString& password = QString());

	QString getCachePath() const;

private:
	QHash<QUrl, Board*> _boards;
	
};

#endif // BOARDMANAGER_H
