#include <QtNetwork/QNetworkRequest>

#include "imagecache.h"
#include "imagenetworkaccessmanager.h"

ImageNetworkAccessManager::ImageNetworkAccessManager(QObject *parent) :
    QNetworkAccessManager(parent)
{
	setCache(new ImageCache);
}

QNetworkReply * ImageNetworkAccessManager::createRequest(Operation op, const QNetworkRequest &request, QIODevice *outgoingData)
{
	QNetworkRequest new_request(request);
	new_request.setAttribute(QNetworkRequest::CacheLoadControlAttribute,
	                         QNetworkRequest::PreferCache);
	return QNetworkAccessManager::createRequest(op, new_request, outgoingData);
}

QNetworkAccessManager * ImageNetworkAccessManagerFactory::create(QObject *parent)
{
	return new ImageNetworkAccessManager(parent);
}
