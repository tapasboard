#include <QtGui/QApplication>
#include <QtNetwork/QNetworkAccessManager>
#include <QtDeclarative/QtDeclarative>
#include "qmlapplicationviewer.h"

#include "global.h"
#include "board.h"
#include "imagenetworkaccessmanager.h"
#include "favoritesmodel.h"
#include "boardmodel.h"
#include "forummodel.h"
#include "topicmodel.h"

BoardManager *board_manager;

static QString adjustPath(const QString &path)
{
#ifdef Q_OS_UNIX
#ifdef Q_OS_MAC
    if (!QDir::isAbsolutePath(path))
        return QString::fromLatin1("%1/../Resources/%2")
                .arg(QCoreApplication::applicationDirPath(), path);
#else
    const QString pathInInstallDir =
            QString::fromLatin1("%1/../%2").arg(QCoreApplication::applicationDirPath(), path);
    if (QFileInfo(pathInInstallDir).exists())
        return pathInInstallDir;
#endif
#endif
    return path;
}

Q_DECL_EXPORT int main(int argc, char *argv[])
{
	QScopedPointer<QApplication> app(createApplication(argc, argv));
	QApplication::setOrganizationDomain("com.javispedro.tapasboard");
	QApplication::setOrganizationName("tapasboard");
	QApplication::setApplicationName("tapasboard");
	QApplication::setApplicationVersion("0.0.1");

	const QString locale_path = adjustPath("i18n");
	QString locale = QLocale::system().name();
    QTranslator translator;

	if (!(translator.load(locale, locale_path))) {
		// Fallback to English
		qDebug() << "Translation not available for" << locale;
		if (!translator.load("en", locale_path)) {
			qDebug() << "Translations not available in" << locale_path;
		}
	}

	app->installTranslator(&translator);

	QScopedPointer<BoardManager> manager(new BoardManager);
	board_manager = manager.data(); // Set the global pointer to this singleton

	QScopedPointer<ImageNetworkAccessManagerFactory> image_nam_factory(new ImageNetworkAccessManagerFactory);

	qmlRegisterUncreatableType<BoardManager>("com.javispedro.tapasboard", 1, 0, "Board",
	                                         "BoardManager must be accessed through boardManager");
	qmlRegisterType<Board>("com.javispedro.tapasboard", 1, 0, "Board");
	qmlRegisterType<FavoritesModel>("com.javispedro.tapasboard", 1, 0, "FavoritesModel");
	qmlRegisterType<BoardModel>("com.javispedro.tapasboard", 1, 0, "BoardModel");
	qmlRegisterType<ForumModel>("com.javispedro.tapasboard", 1, 0, "ForumModel");
	qmlRegisterType<TopicModel>("com.javispedro.tapasboard", 1, 0, "TopicModel");

	QmlApplicationViewer viewer;
	viewer.engine()->setNetworkAccessManagerFactory(image_nam_factory.data());
	viewer.rootContext()->setContextProperty("boardManager", board_manager);
	viewer.setOrientation(QmlApplicationViewer::ScreenOrientationAuto);
	viewer.setMainQmlFile(QLatin1String("qml/main.qml"));
	viewer.showExpanded();

	return app->exec();
}
