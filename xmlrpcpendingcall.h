#ifndef XMLRPCPENDINGCALL_H
#define XMLRPCPENDINGCALL_H

#include <QtCore/QObject>
#include <QtCore/QXmlStreamReader>
#include <QtNetwork/QNetworkReply>

class XmlRpcInterface;

class XmlRpcPendingCall : public QObject
{
	Q_OBJECT
	friend class XmlRpcInterface;
protected:
	explicit XmlRpcPendingCall(QNetworkReply *reply, XmlRpcInterface *parent);

public:
	bool isError() const;
	bool isFinished() const;
	bool isValid() const;

	QVariant value() const;

public slots:
	void waitForFinished();

signals:
	void finished(XmlRpcPendingCall *self);

private:
	bool decodeMethodResponse(QXmlStreamReader* r);
	static QVariant decodeValue(QXmlStreamReader* r);
	static QDateTime decodeISODate(QString text);

private slots:
	void handleRequestFinished();

private:
	QNetworkReply *_reply;
	QVariant _value;
	enum State {
		StateWaitingReply,
		StateNetworkError,
		StateParseError,
		StateReplyReceived,
		StateFaultReceived
	} _state;
};

inline bool XmlRpcPendingCall::isError() const
{
	return _state != StateWaitingReply && _state != StateReplyReceived;
}

inline bool XmlRpcPendingCall::isFinished() const
{
	return _state != StateWaitingReply;
}

inline bool XmlRpcPendingCall::isValid() const
{
	return _state == StateReplyReceived;
}

inline QVariant XmlRpcPendingCall::value() const
{
	return _value;
}

#endif // XMLRPCPENDINGCALL_H
