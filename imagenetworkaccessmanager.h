#ifndef IMAGENETWORKACCESSMANAGER_H
#define IMAGENETWORKACCESSMANAGER_H

#include <QtNetwork/QNetworkAccessManager>
#include <QtDeclarative/QDeclarativeNetworkAccessManagerFactory>

class ImageNetworkAccessManager : public QNetworkAccessManager
{
	Q_OBJECT
public:
	explicit ImageNetworkAccessManager(QObject *parent = 0);

protected:
	QNetworkReply * createRequest(Operation op, const QNetworkRequest &request, QIODevice *outgoingData);
};

class ImageNetworkAccessManagerFactory : public QDeclarativeNetworkAccessManagerFactory
{
public:
	QNetworkAccessManager * create(QObject *parent);
};

#endif // IMAGENETWORKACCESSMANAGER_H
