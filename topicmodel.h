#ifndef TOPICMODEL_H
#define TOPICMODEL_H

#include <QtCore/QAbstractListModel>
#include <QtCore/QDateTime>
#include <QtCore/QUrl>
#include <QtSql/QSqlQuery>

class Board;

class TopicModel : public QAbstractListModel
{
	Q_OBJECT
	Q_PROPERTY(Board * board READ board WRITE setBoard NOTIFY boardChanged)
	Q_PROPERTY(int topicId READ topicId WRITE setTopicId NOTIFY topicIdChanged)
	Q_PROPERTY(int firstUnreadPost READ firstUnreadPost NOTIFY firstUnreadPostChanged)

public:
	TopicModel(QObject *parent = 0);

	enum DataRoles {
		TitleRole = Qt::DisplayRole,
		IconRole = Qt::DecorationRole,
		ContentRole = Qt::ToolTipRole,

		PostIdRole = Qt::UserRole,
		UserIdRole,
		UserNameRole,
		DateTimeRole,
		HumanDateRole,
		HumanTimeRole,
		UnreadRole
	};

	Board * board() const;
	void setBoard(Board * board);

	int topicId() const;
	void setTopicId(const int id);

	int firstUnreadPost() const;

	int rowCount(const QModelIndex &parent = QModelIndex()) const;
	QVariant data(const QModelIndex &index, int role) const;

	bool canFetchMore(const QModelIndex &parent = QModelIndex()) const;
	void fetchMore(const QModelIndex &parent = QModelIndex());

public slots:
	void refresh();
	void markAsRead();

signals:
	void boardChanged();
	void topicIdChanged();
	void firstUnreadPostChanged();

protected:
	struct Post {
		/** Set 'post_id' to -1 for "not yet fetched" */
		int post_id;
		QString title;
		QString content;
		QUrl icon;
		int user_id;
		QString user_name;
		QDateTime time;
		QDateTime last_update_time;
	};

private:
	static QDateTime parseDbDateTime(const QVariant& v);
	static QDateTime oldestPostUpdate(const QList<Post>& posts);
	QDateTime lastTopPostUpdate();
	QList<Post> loadPosts(int start, int end);
	void fetchPost(int position) const; // const because data() calls this
	void enlargeModel(int end);
	void clearModel();

private slots:
	void handleTopicPostsChanged(int topicId, int start, int end);
	void handleTopicPostsUnread(int topicId, int position);
	void update();
	void reload();

private:
	Board *_board;
	int _topicId;
	QList<Post> _data;
	bool _eof;
	int _firstUnread;
};

#endif // TOPICMODEL_H
