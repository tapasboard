#include <QtCore/QDir>
#include <QtCore/QDebug>
#include <QtGui/QDesktopServices>

#include "board.h"
#include "boardmanager.h"

BoardManager::BoardManager(QObject *parent) :
    QObject(parent)
{
}

Board* BoardManager::getBoard(const QUrl &url, const QString& username, const QString& password)
{
	Board *b;
	QHash<QUrl, Board*>::iterator i = _boards.find(url);
	if (i != _boards.end()) {
		b = i.value();
		if (!b->loggedIn() && !username.isEmpty()) {
			// If requested to login but wasn't before, login now.
			b->login(username, password);
		}
	} else {
		b = new Board(url, username, password, this);
		_boards.insert(url, b);
	}
	return b;
}

QString BoardManager::getCachePath() const
{
	QString path = QDesktopServices::storageLocation(QDesktopServices::CacheLocation);

	if (!QDir().mkpath(path)) {
		qWarning() << "Failed to create directory for databases:" << path;
	}

	return path;
}
