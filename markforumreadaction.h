#ifndef MARKFORUMREADACTION_H
#define MARKFORUMREADACTION_H

#include "action.h"

class XmlRpcPendingCall;

class MarkForumReadAction : public Action
{
	Q_OBJECT
public:
	explicit MarkForumReadAction(int forumId, Board *board);

	bool isSupersetOf(Action *action) const;

	void execute();

private slots:
	void handleFinishedCall();

private:
	XmlRpcPendingCall *_call;
	int _forumId;
};

#endif // MARKFORUMREADACTION_H
