#ifndef FETCHPOSTSACTION_H
#define FETCHPOSTSACTION_H

#include <QtCore/QVariant>
#include <QtSql/QSqlQuery>
#include "action.h"

class XmlRpcPendingCall;

class FetchPostsAction : public Action
{
	Q_OBJECT
public:
	enum {
		/** Use as 'end' parameter; 'start' then indicates starting post. */
		FetchAllPosts = -1,
		/** Use as 'start' parameter; 'end' then indicates number of posts. */
		FetchUnreadPosts = -2
	};

	explicit FetchPostsAction(int topicId, int start, int end, Board *board);

	bool isSupersetOf(Action *action) const;

	void execute();

private slots:
	void handleFinishedCall();

private:
	static QString decodePostText(const QVariant& v);
	static QString decodePostTitle(const QVariant& v, const QString& topicTitle);
	QString decodePostContent(const QVariant& v);

private:
	XmlRpcPendingCall *_call;
	int _topicId;
	int _start;
	int _end;
};

#endif // FETCHPOSTSACTION_H
