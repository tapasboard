#ifndef FETCHTOPICSACTION_H
#define FETCHTOPICSACTION_H

#include <QtCore/QVariant>
#include <QtSql/QSqlQuery>

#include "board.h"
#include "action.h"

class XmlRpcPendingCall;

class FetchTopicsAction : public Action
{
	Q_OBJECT
public:
	enum {
		FetchAllTopics = -1
	};

	explicit FetchTopicsAction(int forumId, Board::TopicType type, int start, int end, Board *board);

	bool isSupersetOf(Action *action) const;
	
	void execute();

private slots:
	void handleFinishedCall();

private:
	static QString decodeTopicText(const QVariant& v);
	QList<int> getCurrentDbTopics(int start, int end);

private:
	XmlRpcPendingCall *_call;
	int _forumId;
	Board::TopicType _type;
	int _start;
	int _end;
};

#endif // FETCHTOPICSACTION_H
