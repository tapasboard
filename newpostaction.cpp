#include <QtCore/QDebug>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>

#include "global.h"
#include "board.h"
#include "xmlrpcinterface.h"
#include "xmlrpcreply.h"
#include "newpostaction.h"
#include "fetchpostsaction.h"

NewPostAction::NewPostAction(int topicId, const QString &text, Board *board)
	: Action(board), _topicId(topicId), _text(text)
{
}

bool NewPostAction::isSupersetOf(Action *action) const
{
	Q_UNUSED(action);
	return false;
}

void NewPostAction::execute()
{
	int forum_id = _board->getTopicForumId(_topicId);

	_call = _board->service()->asyncCall("reply_post",
	                                     QString::number(forum_id),
	                                     QString::number(_topicId),
	                                     QByteArray(), // Empty subject
	                                     _text.toUtf8()
	                                     );
	_call->setParent(this);
	connect(_call, SIGNAL(finished(XmlRpcPendingCall*)), SLOT(handleFinishedCall()));
}

void NewPostAction::handleFinishedCall()
{
	XmlRpcReply<QVariantMap> result(_call);
	if (result.isValid()) {
		QVariantMap map = result;
		bool post_ok = map["result"].toBool();
		if (post_ok) {
			int state = map["state"].toInt();
			if (state == 1) {
				// Awaiting moderation
				// TODO
			} else {
				// Refresh posts
				_board->enqueueAction(new FetchPostsAction(_topicId,
				                                           FetchPostsAction::FetchUnreadPosts,
				                                           TOPIC_PAGE_SIZE,
				                                           _board));
			}
		} else {
			qWarning() << "Could not submit post:" << map["result_text"].toString();
		}
	} else {
		qWarning() << "Could not submit post";
		// TODO emit error ...
	}
	emit finished(this);
	_call->deleteLater();
}
