#include <QtCore/QDateTime>
#include <QtCore/QDebug>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>

#include "board.h"
#include "xmlrpcinterface.h"
#include "xmlrpcreply.h"
#include "fetchforumsaction.h"

FetchForumsAction::FetchForumsAction(Board *board) :
    Action(board)
{
}

bool FetchForumsAction::isSupersetOf(Action *action) const
{
	// If 'action' is also a fetch forums list action then yes, this supersets 'action'.
	return qobject_cast<FetchForumsAction*>(action) != 0;
}

void FetchForumsAction::execute()
{
	_call = _board->service()->asyncCall("get_forum");
	_call->setParent(this);
	connect(_call, SIGNAL(finished(XmlRpcPendingCall*)), SLOT(handleFinishedCall()));
}

void FetchForumsAction::handleFinishedCall()
{
	XmlRpcReply<QVariantList> result(_call);
	if (result.isValid()) {
		int order = 0;
		QList<QVariantMap> list = flattenForumList(result, &order);
		QSqlDatabase db = _board->database();
		db.transaction();
		db.exec("DELETE FROM forums");
		if (db.lastError().isValid()) {
			handleDatabaseError("truncating forums table", db.lastError());
		}
		QSqlQuery query(db);
		query.prepare("INSERT INTO forums (forum_id, forum_name, description, parent_id, logo_url, new_post, is_protected, is_subscribed, can_subscribe, url, sub_only, position)"
		              "VALUES (:forum_id, :forum_name, :description, :parent_id, :logo_url, :new_post, :is_protected, :is_subscribed, :can_subscribe, :url, :sub_only, :position)");

		foreach (const QVariant& list_element, list) {
			QVariantMap map = list_element.toMap();
			bool ok = false;
			int forum_id = map["forum_id"].toInt(&ok);
			if (!ok) {
				qWarning() << "No forum_id in" << map;
				continue;
			}
			int parent_id = map["parent_id"].toInt(&ok);
			if (!ok) {
				qWarning() << "No parent_id in" << map;
				continue;
			}

			if (forum_id < 0) {
				qWarning() << "Negative forum_id in" << map;
				continue;
			}
			if (parent_id < 0) {
				// Assume this is a root forum
				parent_id = 0;
			}

			query.bindValue(":forum_id", forum_id);
			query.bindValue(":parent_id", parent_id);
			query.bindValue(":forum_name", decodeForumText(map["forum_name"]));
			query.bindValue(":description", decodeForumText(map["description"]));
			query.bindValue(":logo_url", map["logo_url"].toString());
			query.bindValue(":new_post", map["new_post"].toBool() ? 1 : 0);
			query.bindValue(":is_protected", map["is_protected"].toBool() ? 1 : 0);
			query.bindValue(":is_subscribed", map["is_subscribed"].toBool() ? 1 : 0);
			query.bindValue(":can_subscribe", map["can_subscribed"].toBool() ? 1 : 0);
			query.bindValue(":url", map["url"].toString());
			query.bindValue(":sub_only", map["sub_only"].toBool() ? 1 : 0);
			query.bindValue(":position", map["position"]);

			if (!query.exec()) {
				qWarning() << "Failed to store forum info for:" << forum_id;
				handleDatabaseError("storing forum info", query);
				continue;
			}
		}

		_board->setConfig("last_forums_fetch",
		                  QDateTime::currentDateTimeUtc().toString(Qt::ISODate));
		_board->setConfig("last_forums_fetch_logged_in",
		                  _board->loggedIn() ? "1" : "0");
		db.commit();
		_board->notifyForumsChanged();
	} else {
		qWarning() << "Could not fetch board forums";
		// TODO emit error ...
	}
	emit finished(this);
	_call->deleteLater();
}

QList<QVariantMap> FetchForumsAction::flattenForumList(const QVariantList &list, int *order)
{
	QList<QVariantMap> flattened;
	foreach (const QVariant& list_element, list) {
		QVariantMap map = list_element.toMap();
		map["position"] = (*order)++;
		QVariantMap::iterator child_key = map.find("child");
		if (child_key != map.end()) {
			// There are children, so flatten them too.
			QVariantList sublist = child_key.value().toList();
			map.erase(child_key);
			flattened << map << flattenForumList(sublist, order);
		} else {
			flattened << map;
		}
	}
	return flattened;
}

QString FetchForumsAction::decodeForumText(const QVariant &v)
{
	QByteArray ba = v.toByteArray();
	return QString::fromUtf8(ba.constData(), ba.length());
}
