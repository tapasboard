#ifndef FETCHCONFIGACTION_H
#define FETCHCONFIGACTION_H

#include "action.h"

class XmlRpcPendingCall;

class FetchConfigAction : public Action
{
	Q_OBJECT
public:
	explicit FetchConfigAction(Board *board);
	
	bool isSupersetOf(Action *action) const;

	void execute();

private slots:
	void handleFinishedCall();

private:
	XmlRpcPendingCall *_call;
};

#endif // FETCHCONFIGACTION_H
