#ifndef XMLRPCREPLY_H
#define XMLRPCREPLY_H

#include "xmlrpcpendingcall.h"

template <typename T>
class XmlRpcReply
{
public:
	XmlRpcReply(XmlRpcPendingCall *call);

	bool isValid() const;

	operator T () const;

private:
	bool _valid;
	T _value;
};

template <typename T>
XmlRpcReply<T>::XmlRpcReply(XmlRpcPendingCall *call)
    : _valid(false)
{
	call->waitForFinished();
	if (call->isValid()) {
		QVariant v = call->value();
		if (v.canConvert<T>()) {
			_value = v.value<T>();
			_valid = true;
		}
	}
}

template <typename T>
inline bool XmlRpcReply<T>::isValid() const
{
	return _valid;
}

template <typename T>
inline XmlRpcReply<T>::operator T () const
{
	return _value;
}

#endif // XMLRPCREPLY_H
