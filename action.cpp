#include "board.h"
#include "action.h"

Action::Action(Board *board) :
    QObject(board), _board(board)
{
}

void Action::handleDatabaseError(const char *what, const QSqlError &err)
{
	QString message = QString("Database error while %1: %2").arg(what).arg(err.text());
	emit error(this, message);
}

void Action::handleDatabaseError(const char *what, const QSqlQuery &query)
{
	handleDatabaseError(what, query.lastError());
}
