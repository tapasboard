#include <QtCore/QSettings>
#include <QtCore/QDebug>

#include "favoritesmodel.h"

FavoritesModel::FavoritesModel(QObject *parent) :
    QAbstractListModel(parent)
{
	QHash<int, QByteArray> roles = roleNames();
	roles[NameRole] = QByteArray("title");
	roles[LogoRole] = QByteArray("logo");
	roles[BoardUrlRole] = QByteArray("boardUrl");
	roles[LoginUsernameRole] = QByteArray("loginUsername");
	roles[LoginPasswordRole] = QByteArray("loginPassword");
	setRoleNames(roles);

	load();
	if (_boards.empty()) {
		// Let's load some defaults
		qDebug() << "Setting up some default boards";
		FavoriteBoard board;
		board.name = "Tapatalk Community Forum";
		board.url = "http://support.tapatalk.com/mobiquo/mobiquo.php";
		_boards << board;
		save();
	}
}

int FavoritesModel::rowCount(const QModelIndex &parent) const
{
	if (parent.isValid()) return 0;
	return _boards.size();
}

QVariant FavoritesModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid()) return QVariant();
	const int row = index.row();
	if (row >= _boards.size()) return QVariant();

	switch (role) {
	case NameRole:
		return _boards[row].name;
	case BoardUrlRole:
		return _boards[row].url;
	case LoginUsernameRole:
		return _boards[row].username;
	case LoginPasswordRole:
		return _boards[row].password;
	}

	return QVariant();
}

void FavoritesModel::load()
{
	QSettings settings;
	const int size = settings.beginReadArray("boards");
	_boards.reserve(size);
	for (int i = 0; i < size; i++) {
		settings.setArrayIndex(i);
		FavoriteBoard board;
		board.name = settings.value("name").toString();
		board.url = settings.value("url").toUrl();
		board.username = settings.value("username").toString();
		board.password = settings.value("password").toString();
		_boards.append(board);
	}
	settings.endArray();
}

void FavoritesModel::save()
{
	QSettings settings;
	const int size = _boards.size();
	settings.beginWriteArray("boards", size);
	for (int i = 0; i < size; i++) {
		settings.setArrayIndex(i);
		settings.setValue("name", _boards[i].name);
		settings.setValue("url", _boards[i].url);
		settings.setValue("username", _boards[i].username);
		settings.setValue("password", _boards[i].password);
	}
	settings.endArray();
}
