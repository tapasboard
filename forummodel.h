#ifndef FORUMMODEL_H
#define FORUMMODEL_H

#include <QtCore/QAbstractListModel>
#include <QtCore/QDateTime>
#include <QtSql/QSqlQuery>

#include "board.h"

class ForumModel : public QAbstractListModel
{
	Q_OBJECT
	Q_PROPERTY(Board * board READ board WRITE setBoard NOTIFY boardChanged)
	Q_PROPERTY(int forumId READ forumId WRITE setForumId NOTIFY forumIdChanged)

public:
	ForumModel(QObject *parent = 0);

	enum DataRoles {
		TitleRole = Qt::DisplayRole,
		IconRole = Qt::DecorationRole,

		TopicIdRole = Qt::UserRole,
		TopicTypeRole,
		LastReplyTimeRole,
		NumRepliesRole,
		RelativeDateRole,
		UnreadRole
	};

	Board * board() const;
	void setBoard(Board *board);

	int forumId() const;
	void setForumId(const int id);

	int rowCount(const QModelIndex &parent = QModelIndex()) const;
	QVariant data(const QModelIndex &index, int role) const;

	bool canFetchMore(const QModelIndex &parent = QModelIndex()) const;
	void fetchMore(const QModelIndex &parent = QModelIndex());

public slots:
	void refresh();

signals:
	void boardChanged();
	void forumIdChanged();

protected:
	struct Topic {
		int topic_id;
		Board::TopicType type;
		QString title;
		int num_replies;
		bool unread;
		QDateTime last_reply_time;
		QDateTime last_update_time;
	};

private:
	static QDateTime parseDateTime(const QVariant& v);
	static QDateTime oldestPostUpdate(const QList<Topic>& topics);
	QDateTime lastTopPostUpdate();
	QList<Topic> loadTopics(Board::TopicType type, int start, int end);
	void fetchTopic(int position) const; // const because data() calls this
	void clearModel();

private slots:
	void handleForumTopicsChanged(int forumId, Board::TopicType type, int start, int end);
	void handleForumTopicChanged(int forumId, int topicId);
	void update();
	void reload();

private:
	Board *_board;
	int _forumId;
	QList<Topic> _data;
	int _numAnnouncements;
	int _numSticky;
	bool _eof;
};


#endif // FORUMMODEL_H
