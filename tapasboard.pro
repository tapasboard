# Add more folders to ship with the application, here
qml_folder.source = qml
qml_folder.target = .
i18n_folder.source = i18n
i18n_folder.target = .
DEPLOYMENTFOLDERS = qml_folder i18n_folder

# Additional import path used to resolve QML modules in Creator's code model
QML_IMPORT_PATH =

symbian:TARGET.UID3 = 0xE33C9CB4

# Smart Installer package's UID
# This UID is from the protected range and therefore the package will
# fail to install if self-signed. By default qmake uses the unprotected
# range value if unprotected UID is defined for the application and
# 0x2002CCCF value if protected UID is given to the application
#symbian:DEPLOYMENT.installer_header = 0x2002CCCF

# Allow network access on Symbian
symbian:TARGET.CAPABILITY += NetworkServices

# Extra QT modules required
QT += network sql

# If your application uses the Qt Mobility libraries, uncomment the following
# lines and add the respective components to the MOBILITY variable.
# CONFIG += mobility
# MOBILITY +=

# Speed up launching on MeeGo/Harmattan when using applauncherd daemon
CONFIG += qdeclarative-boostable

# Add dependency to Symbian components
# CONFIG += qt-components

SOURCES += main.cpp \
    action.cpp \
    board.cpp \
    boardmanager.cpp \
    fetchconfigaction.cpp \
    xmlrpcinterface.cpp \
    xmlrpcpendingcall.cpp \
    fetchforumsaction.cpp \
    boardmodel.cpp \
    forummodel.cpp \
    fetchtopicsaction.cpp \
    topicmodel.cpp \
    fetchpostsaction.cpp \
    favoritesmodel.cpp \
    imagecache.cpp \
    imagenetworkaccessmanager.cpp \
    loginaction.cpp \
    markforumreadaction.cpp \
    newpostaction.cpp \
    newtopicaction.cpp

HEADERS += \
    action.h \
    board.h \
    boardmanager.h \
    fetchconfigaction.h \
    xmlrpcinterface.h \
    xmlrpcpendingcall.h \
    xmlrpcreply.h \
    fetchforumsaction.h \
    global.h \
    boardmodel.h \
    forummodel.h \
    fetchtopicsaction.h \
    topicmodel.h \
    fetchpostsaction.h \
    favoritesmodel.h \
    imagecache.h \
    imagenetworkaccessmanager.h \
    loginaction.h \
    markforumreadaction.h \
    newpostaction.h \
    newtopicaction.h

TRANSLATIONS += i18n/en.ts i18n/es.ts

# Uncomment this while working on the translations
# lupdate_hack { SOURCES += qml/*.qml }

OTHER_FILES += \
    qtc_packaging/debian_harmattan/rules \
    qtc_packaging/debian_harmattan/README \
    qtc_packaging/debian_harmattan/manifest.aegis \
    qtc_packaging/debian_harmattan/copyright \
    qtc_packaging/debian_harmattan/control \
    qtc_packaging/debian_harmattan/compat \
    qtc_packaging/debian_harmattan/changelog

# Please do not modify the following two lines. Required for deployment.
include(qmlapplicationviewer/qmlapplicationviewer.pri)
qtcAddDeployment()
