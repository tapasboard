#include <QtCore/QDir>
#include <QtCore/QDebug>

#include "global.h"
#include "imagecache.h"

ImageCache::ImageCache(QObject *parent) :
    QNetworkDiskCache(parent)
{
	QString cache_path = board_manager->getCachePath() + "/images";
	QDir dir;
	if (!dir.mkpath(cache_path)) {
		qWarning() << "Could not create image cache path" << cache_path;
	}
	setCacheDirectory(cache_path);
}
