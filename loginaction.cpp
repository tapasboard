#include <QtCore/QDebug>

#include "board.h"
#include "xmlrpcinterface.h"
#include "xmlrpcreply.h"
#include "loginaction.h"

LoginAction::LoginAction(const QString &name, const QString &password, Board *board)
	: Action(board), _username(name), _password(password)
{
}

bool LoginAction::isSupersetOf(Action *action) const
{
	LoginAction *other = qobject_cast<LoginAction*>(action);
	if (other) {
		if (other->_username == _username && other->_password == _password) {
			return true;
		}
	}
	return false;
}

void LoginAction::execute()
{
	qDebug() << "Trying to login as" << _username;
	_call = _board->service()->asyncCall("login",
	                                     _username.toUtf8(), _password.toUtf8());
	_call->setParent(this);
	connect(_call, SIGNAL(finished(XmlRpcPendingCall*)), SLOT(handleFinishedCall()));
}

void LoginAction::handleFinishedCall()
{
	XmlRpcReply<QVariantMap> result(_call);
	if (result.isValid()) {
		QVariantMap map = result;
		bool login_ok = map["result"].toBool();
		if (login_ok) {
			_board->notifyLogin(map);
		} else {
			qWarning() << "Could not login to board as:" << _username;
		}
	} else {
		qWarning() << "Could not login";
		// TODO emit error ...
	}
	emit finished(this);
	_call->deleteLater();
}
