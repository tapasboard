#ifndef IMAGECACHE_H
#define IMAGECACHE_H

#include <QtNetwork/QNetworkDiskCache>

class ImageCache : public QNetworkDiskCache
{
	Q_OBJECT
public:
	explicit ImageCache(QObject *parent = 0);
};

#endif // IMAGECACHE_H
