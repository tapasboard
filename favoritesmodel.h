#ifndef FAVORITESMODEL_H
#define FAVORITESMODEL_H

#include <QtCore/QAbstractListModel>
#include <QtCore/QUrl>

class FavoritesModel : public QAbstractListModel
{
	Q_OBJECT
public:
	explicit FavoritesModel(QObject *parent = 0);

	enum DataRoles {
		NameRole = Qt::DisplayRole,
		LogoRole = Qt::DecorationRole,

		BoardUrlRole = Qt::UserRole,
		LoginUsernameRole,
		LoginPasswordRole
	};

	int rowCount(const QModelIndex &parent = QModelIndex()) const;
	QVariant data(const QModelIndex &index, int role) const;

protected:
	struct FavoriteBoard {
		QString name;
		QUrl url;
		QString username;
		QString password;
	};

private:
	void load();
	void save();

private:

	QList<FavoriteBoard> _boards;
};

#endif // FAVORITESMODEL_H
