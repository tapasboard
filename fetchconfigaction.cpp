#include <QtCore/QDateTime>
#include <QtCore/QDebug>
#include <QtSql/QSqlQuery>

#include "board.h"
#include "xmlrpcinterface.h"
#include "xmlrpcreply.h"
#include "fetchconfigaction.h"

FetchConfigAction::FetchConfigAction(Board *board) :
    Action(board)
{
}

bool FetchConfigAction::isSupersetOf(Action *action) const
{
	// If 'action' is also a fetch board config action then yes, this supersets 'action'.
	return qobject_cast<FetchConfigAction*>(action) != 0;
}

void FetchConfigAction::execute()
{
	_call = _board->service()->asyncCall("get_config");
	_call->setParent(this);
	connect(_call, SIGNAL(finished(XmlRpcPendingCall*)), SLOT(handleFinishedCall()));
}

void FetchConfigAction::handleFinishedCall()
{
	XmlRpcReply<QVariantMap> result(_call);
	if (result.isValid()) {
		QVariantMap map = result;
		QSqlDatabase db = _board->database();
		db.transaction();
		QSqlQuery query(db);

		// Let's add some of our config settings
		map["last_config_fetch"] = QDateTime::currentDateTimeUtc().toString(Qt::ISODate);
		map["tapasboard_db_version"] = Board::CURRENT_DB_VERSION;

		query.prepare("INSERT OR REPLACE INTO config (key, value) VALUES (:key, :value)");
		for (QVariantMap::iterator i = map.begin(); i != map.end(); i++) {
			query.bindValue(":key", i.key());
			query.bindValue(":value", i.value().toString());
			if (!query.exec()) {
				qWarning() << "Failed to set config key:" << i.key();
			}
		}
		db.commit();
		_board->notifyConfigChanged();
	} else {
		qWarning() << "Could not fetch board configuration";
		// TODO emit error ...
	}
	emit finished(this);
	_call->deleteLater();
}
