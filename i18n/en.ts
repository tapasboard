<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
<context>
    <name>Board</name>
    <message>
        <location filename="../board.cpp" line="257"/>
        <source>Today</source>
        <translation>Today</translation>
    </message>
    <message>
        <location filename="../board.cpp" line="259"/>
        <source>Yesterday</source>
        <translation>Yesterday</translation>
    </message>
    <message>
        <location filename="../board.cpp" line="238"/>
        <source>Just now</source>
        <translation>Just now</translation>
    </message>
    <message numerus="yes">
        <location filename="../board.cpp" line="240"/>
        <source>%n second(s) ago</source>
        <translation>
            <numerusform>%n seconds ago</numerusform>
            <numerusform>%n second ago</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../board.cpp" line="243"/>
        <source>%n minute(s) ago</source>
        <translation>
            <numerusform>%n minute ago</numerusform>
            <numerusform>%n minutes ago</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../board.cpp" line="251"/>
        <source>Today %1</source>
        <translation>Today %1</translation>
    </message>
    <message>
        <location filename="../board.cpp" line="253"/>
        <source>Yesterday %1</source>
        <translation>Yesterday %1</translation>
    </message>
    <message numerus="yes">
        <location filename="../board.cpp" line="270"/>
        <source>%n week(s) ago</source>
        <translation>
            <numerusform>%n week ago</numerusform>
            <numerusform>%n weeks ago</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../board.cpp" line="272"/>
        <source>%n month(s) ago</source>
        <translation>
            <numerusform>%n month ago</numerusform>
            <numerusform>%n months ago</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../board.cpp" line="274"/>
        <source>%n year(s) ago</source>
        <translation>
            <numerusform>%n year ago</numerusform>
            <numerusform>%n years ago</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>BoardPage</name>
    <message>
        <location filename="../qml/BoardPage.qml" line="40"/>
        <source>Mark all forums read</source>
        <translation>Mark all forums read</translation>
    </message>
    <message>
        <location filename="../qml/BoardPage.qml" line="40"/>
        <source>Mark subforums read</source>
        <translation>Mark subforums read</translation>
    </message>
</context>
<context>
    <name>ForumModel</name>
    <message>
        <location filename="../forummodel.cpp" line="105"/>
        <source>Announcement</source>
        <translation>Announcement</translation>
    </message>
    <message>
        <location filename="../forummodel.cpp" line="107"/>
        <source>Sticky</source>
        <translation>Sticky</translation>
    </message>
</context>
<context>
    <name>NewPostSheet</name>
    <message>
        <location filename="../qml/NewPostSheet.qml" line="15"/>
        <source>Submit</source>
        <translation>Submit</translation>
    </message>
    <message>
        <location filename="../qml/NewPostSheet.qml" line="16"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../qml/NewPostSheet.qml" line="30"/>
        <source>Write your reply here</source>
        <translation>Write your reply here</translation>
    </message>
</context>
<context>
    <name>NewTopicSheet</name>
    <message>
        <location filename="../qml/NewTopicSheet.qml" line="15"/>
        <source>Submit</source>
        <translation>Submit</translation>
    </message>
    <message>
        <location filename="../qml/NewTopicSheet.qml" line="16"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../qml/NewTopicSheet.qml" line="39"/>
        <source>Subject:</source>
        <translation>Subject:</translation>
    </message>
    <message>
        <location filename="../qml/NewTopicSheet.qml" line="57"/>
        <source>Write subject here</source>
        <translation>Write subject here</translation>
    </message>
    <message>
        <location filename="../qml/NewTopicSheet.qml" line="89"/>
        <source>Write your message here</source>
        <translation>Write your message here</translation>
    </message>
</context>
<context>
    <name>TopicPage</name>
    <message>
        <location filename="../qml/TopicPage.qml" line="56"/>
        <source>Go to first post</source>
        <translation>Go to first post</translation>
    </message>
    <message>
        <location filename="../qml/TopicPage.qml" line="60"/>
        <source>Go to last post</source>
        <translation>Go to last post</translation>
    </message>
</context>
</TS>
