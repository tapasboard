<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="es_ES">
<context>
    <name>Board</name>
    <message>
        <location filename="../board.cpp" line="257"/>
        <source>Today</source>
        <translation>Hoy</translation>
    </message>
    <message>
        <location filename="../board.cpp" line="259"/>
        <source>Yesterday</source>
        <translation>Ayer</translation>
    </message>
    <message>
        <location filename="../board.cpp" line="238"/>
        <source>Just now</source>
        <translation>Ahora mismo</translation>
    </message>
    <message numerus="yes">
        <location filename="../board.cpp" line="240"/>
        <source>%n second(s) ago</source>
        <translation>
            <numerusform>Hace %n segundo</numerusform>
            <numerusform>Hace %n segundos</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../board.cpp" line="243"/>
        <source>%n minute(s) ago</source>
        <translation>
            <numerusform>Hace %n minuto</numerusform>
            <numerusform>Hace %n minutos</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../board.cpp" line="251"/>
        <source>Today %1</source>
        <translation>Hoy, %1</translation>
    </message>
    <message>
        <location filename="../board.cpp" line="253"/>
        <source>Yesterday %1</source>
        <translation>Ayer, %1</translation>
    </message>
    <message numerus="yes">
        <location filename="../board.cpp" line="270"/>
        <source>%n week(s) ago</source>
        <translation>
            <numerusform>Hace %n semana</numerusform>
            <numerusform>Hace %n semanas</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../board.cpp" line="272"/>
        <source>%n month(s) ago</source>
        <translation>
            <numerusform>Hace %n mes</numerusform>
            <numerusform>Hace %n meses</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../board.cpp" line="274"/>
        <source>%n year(s) ago</source>
        <translation>
            <numerusform>Hace %n año</numerusform>
            <numerusform>Hace %n años</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>BoardPage</name>
    <message>
        <location filename="../qml/BoardPage.qml" line="40"/>
        <source>Mark all forums read</source>
        <translation>Marcar foros como leídos</translation>
    </message>
    <message>
        <location filename="../qml/BoardPage.qml" line="40"/>
        <source>Mark subforums read</source>
        <translation>Marcar subforos como leídos</translation>
    </message>
</context>
<context>
    <name>ForumModel</name>
    <message>
        <location filename="../forummodel.cpp" line="105"/>
        <source>Announcement</source>
        <translation>Anuncio</translation>
    </message>
    <message>
        <location filename="../forummodel.cpp" line="107"/>
        <source>Sticky</source>
        <translation>Fijo</translation>
    </message>
</context>
<context>
    <name>NewPostSheet</name>
    <message>
        <location filename="../qml/NewPostSheet.qml" line="15"/>
        <source>Submit</source>
        <translation>Enviar</translation>
    </message>
    <message>
        <location filename="../qml/NewPostSheet.qml" line="16"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../qml/NewPostSheet.qml" line="30"/>
        <source>Write your reply here</source>
        <translation>Escribe aquí tu respuesta</translation>
    </message>
</context>
<context>
    <name>NewTopicSheet</name>
    <message>
        <location filename="../qml/NewTopicSheet.qml" line="15"/>
        <source>Submit</source>
        <translation>Enviar</translation>
    </message>
    <message>
        <location filename="../qml/NewTopicSheet.qml" line="16"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../qml/NewTopicSheet.qml" line="39"/>
        <source>Subject:</source>
        <translation>Tema:</translation>
    </message>
    <message>
        <location filename="../qml/NewTopicSheet.qml" line="57"/>
        <source>Write subject here</source>
        <translation>Escribe el tema aquí</translation>
    </message>
    <message>
        <location filename="../qml/NewTopicSheet.qml" line="89"/>
        <source>Write your message here</source>
        <translation>Escribe tu mensaje aquí</translation>
    </message>
</context>
<context>
    <name>TopicPage</name>
    <message>
        <location filename="../qml/TopicPage.qml" line="56"/>
        <source>Go to first post</source>
        <translation>Ir al primer mensaje</translation>
    </message>
    <message>
        <location filename="../qml/TopicPage.qml" line="60"/>
        <source>Go to last post</source>
        <translation>Ir al último mensaje</translation>
    </message>
</context>
</TS>
