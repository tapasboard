#ifndef XMLRPCINTERFACE_H
#define XMLRPCINTERFACE_H

#include <QtCore/QObject>
#include <QtCore/QVariant>
#include <QtCore/QUrl>
#include <QtCore/QXmlStreamWriter>
#include <QtNetwork/QNetworkAccessManager>

#include "xmlrpcpendingcall.h"

#define XML_RPC_DEBUG 0

class XmlRpcInterface : public QObject
{
	Q_OBJECT
public:
	explicit XmlRpcInterface(const QUrl& endpoint, QObject *parent = 0);

	bool isAccessible() const;

	XmlRpcPendingCall *asyncCall(const QString& method,
	                            const QVariant &arg1 = QVariant(),
	                            const QVariant &arg2 = QVariant(),
	                            const QVariant &arg3 = QVariant(),
	                            const QVariant &arg4 = QVariant(),
	                            const QVariant &arg5 = QVariant(),
	                            const QVariant &arg6 = QVariant(),
	                            const QVariant &arg7 = QVariant(),
	                            const QVariant &arg8 = QVariant());

	XmlRpcPendingCall *asyncCallWithArgumentList(const QString& method,
	                                            const QList<QVariant>& args);

private:
	static QByteArray encodeCall(const QString& method, const QList<QVariant>& args);
	static void encodeValue(QXmlStreamWriter* w, const QVariant& value);

private:
	QUrl _endpoint;
	QNetworkAccessManager *_manager;
	
};

inline bool XmlRpcInterface::isAccessible() const
{
	return _manager->networkAccessible() != QNetworkAccessManager::NotAccessible;
}

inline XmlRpcPendingCall *XmlRpcInterface::asyncCall(const QString &method,
                                                    const QVariant &arg1,
                                                    const QVariant &arg2,
                                                    const QVariant &arg3,
                                                    const QVariant &arg4,
                                                    const QVariant &arg5,
                                                    const QVariant &arg6,
                                                    const QVariant &arg7,
                                                    const QVariant &arg8)
{
	QList<QVariant> args;
	if (arg1.isValid()) args << arg1;
	if (arg2.isValid()) args << arg2;
	if (arg3.isValid()) args << arg3;
	if (arg4.isValid()) args << arg4;
	if (arg5.isValid()) args << arg5;
	if (arg6.isValid()) args << arg6;
	if (arg7.isValid()) args << arg7;
	if (arg8.isValid()) args << arg8;
	return asyncCallWithArgumentList(method, args);
}

#endif // XMLRPCINTERFACE_H
