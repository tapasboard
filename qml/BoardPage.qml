import QtQuick 1.1
import com.nokia.meego 1.1
import com.nokia.extras 1.1
import com.javispedro.tapasboard 1.0

Page {
	id: boardPage

	anchors.leftMargin: UiConstants.DefaultMargin
	anchors.rightMargin: UiConstants.DefaultMargin

	property Board board : null;
	property int forumId;

	tools: ToolBarLayout {
		ToolIcon {
			platformIconId: "toolbar-back"
			onClicked: pageStack.pop()
		}
		ToolIcon {
			platformIconId: board.busy ? "toolbar-cancle" : "toolbar-refresh"
			onClicked: {
				if (board.busy) {
					board.cancelAllActions();
				} else {
					boardModel.refresh();
				}
			}
		}
		ToolIcon {
			platformIconId: "toolbar-view-menu"
			onClicked: (boardMenu.status === DialogStatus.Closed) ? boardMenu.open() : boardMenu.close()
		}
	}

	Menu {
		id: boardMenu
		MenuLayout {
			MenuItem {
				text: boardPage.forumId == 0 ? qsTr("Mark all forums read") : qsTr("Mark subforums read");
				onClicked: boardModel.markSubforumsRead();
			}
		}

	}

	ListView {
		id: forumsView
		anchors.fill: parent
		model: BoardModel {
			id: boardModel
			board: boardPage.board
			forumId: boardPage.forumId
		}
		section.criteria: ViewSection.FullString
		section.property: "category"
		section.delegate: GroupHeader {
			width: parent.width
			text: section
		}

		delegate: EmptyListDelegate {
			id: forumItem

			height: Math.max(forumItemColumn.height + UiConstants.ButtonSpacing * 2, UiConstants.ListItemHeightDefault)

			unread: model.unread

			Column {
				id: forumItemColumn
				anchors.left: parent.left
				anchors.right: forumItemImage.left
				anchors.verticalCenter: parent.verticalCenter

				Text {
					text: model.title
					width: parent.width
					font: UiConstants.TitleFont
				}

				Text {
					text: model.description
					width: parent.width
					font: UiConstants.SubtitleFont
					wrapMode: Text.Wrap
					visible: text != ""
				}
			}

			Image {
				id: forumItemImage
				source: "image://theme/icon-m-common-drilldown-arrow" + (theme.inverted ? "-inverse" : "")
				anchors.right: parent.right
				anchors.verticalCenter: parent.verticalCenter
			}

			onClicked: {
				if (model.subOnly) {
					pageStack.push(Qt.resolvedUrl("BoardPage.qml"), {
										  board: boardPage.board,
										  forumId: model.forumId
										});
				} else {
					pageStack.push(Qt.resolvedUrl("ForumPage.qml"), {
										  board: boardPage.board,
										  forumId: model.forumId
										});
				}
			}
		}
	}

	ScrollDecorator {
		flickableItem: forumsView
	}

	BusyIndicator {
		anchors.centerIn: parent
		platformStyle: BusyIndicatorStyle { size: "large" }
		visible: forumsView.count == 0 && board.busy
		running: visible
	}
}
