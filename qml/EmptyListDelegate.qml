import QtQuick 1.1
import com.nokia.meego 1.1

Item {
	id: listItem

	signal clicked
	property alias pressed: mouseArea.pressed
	property bool unread: false

	anchors.left: parent.left
	anchors.right: parent.right
	anchors.leftMargin: UiConstants.ButtonSpacing
	height: UiConstants.ListItemHeightDefault

	BorderImage {
		id: background
		anchors.fill: parent
		anchors.leftMargin: -(UiConstants.DefaultMargin+parent.anchors.leftMargin)
		anchors.rightMargin: -UiConstants.DefaultMargin
		border { left: 22; right: 22; top: 22; bottom: 22; }
		visible: pressed || unread
		source: "image://theme/meegotouch" + (unread|(!unread&&!pressed)?"-unread-inbox":"")
				+ "-panel" + (theme.inverted?"-inverted":"")
				+ "-background" + (pressed?"-pressed":"")
	}

	MouseArea {
		id: mouseArea;
		anchors.fill: parent
		onClicked: {
			listItem.clicked();
		}
	}
}
