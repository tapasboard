import QtQuick 1.1
import com.nokia.meego 1.1
import com.nokia.extras 1.1
import com.javispedro.tapasboard 1.0

Sheet {
	id: newPostSheet

	property Board board;
	property int topicId;

	anchors.leftMargin: UiConstants.DefaultMargin
	anchors.rightMargin: UiConstants.DefaultMargin

	acceptButtonText: qsTr("Submit")
	rejectButtonText: qsTr("Cancel")

	content: TextArea {
		id: postText
		anchors {
			fill: parent
		}

		platformStyle: TextAreaStyle {
			background: "image://theme/meegotouch-sheet-inputfield-background"
			backgroundSelected: background
			backgroundDisabled: ""
		}

		placeholderText: qsTr("Write your reply here")
		focus: true
		wrapMode: TextEdit.Wrap
	}

	onAccepted: {
		board.replyToTopic(topicId, postText.text)
	}
}
