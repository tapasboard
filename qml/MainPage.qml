import QtQuick 1.1
import com.nokia.meego 1.1
import com.nokia.extras 1.1
import com.javispedro.tapasboard 1.0

Page {
	id: mainPage

	anchors.leftMargin: UiConstants.DefaultMargin
	anchors.rightMargin: UiConstants.DefaultMargin

	ListView {
		id: favoritesView
		anchors.fill: parent
		model: FavoritesModel {

		}
		delegate: ListDelegate {
			id: favoriteItem

			Image {
				id: topicItemImage
				source: "image://theme/icon-m-common-drilldown-arrow" + (theme.inverted ? "-inverse" : "")
				anchors.right: parent.right
				anchors.verticalCenter: parent.verticalCenter
			}

			onClicked: {
				var board = boardManager.getBoard(model.boardUrl,
												  model.loginUsername,
												  model.loginPassword);
				pageStack.push(Qt.resolvedUrl("BoardPage.qml"), {
				                      board: board,
				                      forumId: board.rootForumId
								  });
			}
		}
	}
}
