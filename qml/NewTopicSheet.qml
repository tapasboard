import QtQuick 1.1
import com.nokia.meego 1.1
import com.nokia.extras 1.1
import com.javispedro.tapasboard 1.0

Sheet {
	id: newTopicSheet

	property Board board;
	property int forumId;

	anchors.leftMargin: UiConstants.DefaultMargin
	anchors.rightMargin: UiConstants.DefaultMargin

	acceptButtonText: qsTr("Submit")
	rejectButtonText: qsTr("Cancel")

	content: Flickable {
		anchors.fill: parent
		contentWidth: parent.width
		contentHeight: subjectLine.height + postText.height

		Item {
			id: subjectLine
			anchors {
				top: parent.top
				left: parent.left
				right: parent.right
			}
			height: childrenRect.height - 1

			Label {
				id: subjectLabel
				anchors {
					margins: UiConstants.DefaultMargin - 2
					top: parent.top
					left: parent.left
				}
				text: qsTr("Subject:")
			}

			TextArea {
				id: subjectText
				anchors {
					top: parent.top
					left: subjectLabel.right
					right: parent.right
				}

				platformStyle: TextAreaStyle {
					background: ""
					backgroundSelected: ""
					backgroundDisabled: ""
					textFont.bold: true
				}

				placeholderText: qsTr("Write subject here")
				wrapMode: TextEdit.Wrap
			}


			BorderImage {
				anchors {
					top: subjectText.bottom
					left: parent.left
					right: parent.right
				}

				height: 3
				source: "image://theme/meegotouch-sheet-editor-collapsed-background"
			}
		}

		TextArea {
			id: postText
			anchors {
				top: subjectLine.bottom
				left: parent.left
				right: parent.right
			}
			height: Math.max(400, implicitHeight)

			platformStyle: TextAreaStyle {
				background: "image://theme/meegotouch-sheet-inputfield-background"
				backgroundSelected: background
				backgroundDisabled: ""
			}

			placeholderText: qsTr("Write your message here")
			wrapMode: TextEdit.Wrap
		}
	}

	onAccepted: {
		board.newTopic(forumId, subjectText.text, postText.text);
	}
}
