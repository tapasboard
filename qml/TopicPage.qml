import QtQuick 1.1
import com.nokia.meego 1.1
import com.nokia.extras 1.1
import com.javispedro.tapasboard 1.0

Page {
	id: topicPage

	anchors.leftMargin: UiConstants.DefaultMargin
	anchors.rightMargin: UiConstants.DefaultMargin

	property Board board;
	property int topicId;

	/** -1: Do not keep view; -2: Keep view on last post; >0: Keep view on a post. */
	property int _keepView: -1;

	tools: ToolBarLayout {
		ToolIcon {
			platformIconId: "toolbar-back"
			onClicked: pageStack.pop()
		}
		ToolIcon {
			platformIconId: "toolbar-add"
			onClicked: {
				var component = Qt.createComponent("NewPostSheet.qml");
				if (component.status === Component.Ready) {
					var sheet = component.createObject(topicPage, {
														   "board": board,
														   "topicId": topicId
													   });
					sheet.open();
				}
			}
		}
		ToolIcon {
			platformIconId: board.busy ? "toolbar-cancle" : "toolbar-refresh"
			onClicked: {
				if (board.busy) {
					board.cancelAllActions();
				} else {
					topicModel.refresh();
				}
			}
		}
		ToolIcon {
			platformIconId: "toolbar-view-menu"
			onClicked: (topicMenu.status === DialogStatus.Closed) ? topicMenu.open() : topicMenu.close()
		}
	}

	Menu {
		id: topicMenu
		MenuLayout {
			MenuItem {
				text: qsTr("Go to first post");
				onClicked: postsView.positionViewAtIndex(0, ListView.Beginning);
			}
			MenuItem {
				text: qsTr("Go to last post")
				onClicked: {
					_keepView = -2; // Keep on last post
					postsView.positionViewAtEnd();
				}
			}
		}

	}

	ListView {
		id: postsView
		anchors.fill: parent
		model: TopicModel {
			id: topicModel
			board: topicPage.board
			topicId: topicPage.topicId

			onFirstUnreadPostChanged: {
				postsView.positionViewAtIndex(firstUnreadPost, ListView.Beginning);
				topicModel.markAsRead();
				_keepView = firstUnreadPost;
			}
		}
		section.property: "humanDate"
		section.criteria: ViewSection.FullString
		section.delegate: GroupHeader {
			width: parent.width
			text: section
		}

		cacheBuffer: 200

		delegate: Item {
			id: postItem

			height: postItemRectangle.height + UiConstants.ButtonSpacing * 2
			width: parent.width

			Rectangle {
				id: postItemRectangle
				width: parent.width
				height: postItemColumn.height + UiConstants.DefaultMargin
				anchors.centerIn: parent

				visible: model.postId >= 0
				color: "white"
				radius: 20

				border.width: model.unread ? 2 : 0
				border.color: "black"

				Column {
					id: postItemColumn
					anchors.left: parent.left
					anchors.right: parent.right
					anchors.margins: UiConstants.DefaultMargin
					anchors.verticalCenter: parent.verticalCenter
					spacing: 2

					Item {
						id: authorInfo

						width: parent.width
						height: childrenRect.height

						Image {
							id: authorIcon
							height: 48
							width: 48
							fillMode: Image.PreserveAspectFit
							source: model.icon
							sourceSize.height: 48
						}

						Text {
							id: authorName
							text: model.userName
							font: UiConstants.SmallTitleFont
							textFormat: Text.PlainText
						}

						Text {
							id: authorTime
							anchors.top: parent.top
							anchors.right: parent.right
							text: model.humanTime
							font: UiConstants.SubtitleFont
							textFormat: Text.PlainText
						}

						states: State {
							name: "with-image"
							when: authorIcon.status === Image.Ready

							AnchorChanges {
								target: authorName
								anchors.bottom: authorIcon.bottom
								anchors.left: authorIcon.right
							}

							PropertyChanges {
								target: authorName
								anchors.leftMargin: UiConstants.ButtonSpacing
							}
						}
					}

					Text {
						text: model.title
						width: parent.width
						font: UiConstants.TitleFont
						visible: text != ""
						textFormat: Text.PlainText
						wrapMode: Text.Wrap
					}

					Text {
						text: model.content
						width: parent.width
						font: UiConstants.SubtitleFont
						textFormat: Text.RichText
						wrapMode: Text.Wrap
						onLinkActivated: Qt.openUrlExternally(link)
					}
				}
			}
		}
	}

	Connections {
		target: _keepView !== -1 ? postsView : null
		onMovementStarted: {
			_keepView = -1; // Disable keep view
		}
		onContentHeightChanged: {
			if (_keepView == -2) {
				postsView.positionViewAtEnd();
			} else if (_keepView >= 0) {
				postsView.positionViewAtIndex(_keepView, ListView.Beginning);
			}
		}
	}

	ScrollDecorator {
		flickableItem: postsView
	}

	BusyIndicator {
		anchors.centerIn: parent
		platformStyle: BusyIndicatorStyle { size: "large" }
		visible: postsView.count == 0 && board.busy
		running: visible
	}
}
