import QtQuick 1.1
import com.nokia.meego 1.1
import com.nokia.extras 1.1
import com.javispedro.tapasboard 1.0

Page {
	id: forumPage

	anchors.leftMargin: UiConstants.DefaultMargin
	anchors.rightMargin: UiConstants.DefaultMargin

	property Board board: null;
	property int forumId;

	tools: ToolBarLayout {
		ToolIcon {
			platformIconId: "toolbar-back"
			onClicked: pageStack.pop()
		}
		ToolIcon {
			platformIconId: "toolbar-add"
			onClicked: {
				var component = Qt.createComponent("NewTopicSheet.qml");
				if (component.status === Component.Ready) {
					var sheet = component.createObject(forumPage, {
														   "board": board,
														   "forumId": forumId
													   });
					sheet.open();
				}
			}
		}
		ToolIcon {
			platformIconId: board.busy ? "toolbar-cancle" : "toolbar-refresh"
			onClicked: {
				if (board.busy) {
					board.cancelAllActions();
				} else {
					forumModel.refresh();
				}
			}
		}
		ToolIcon {
			platformIconId: "toolbar-view-menu"
		}
	}

	ListView {
		id: topicsView
		anchors.fill: parent
		model: ForumModel {
			id: forumModel
			board: forumPage.board
			forumId: forumPage.forumId
		}

		section.property: "relativeDate"
		section.criteria: ViewSection.FullString
		section.delegate: GroupHeader {
			width: parent.width
			text: section
		}

		delegate: EmptyListDelegate {
			id: topicItem

			height: Math.max(topicItemColumn.height + UiConstants.ButtonSpacing * 2, UiConstants.ListItemHeightDefault)

			unread: model.unread

			Column {
				id: topicItemColumn
				anchors.left: parent.left
				anchors.right: topicRepliesCount.left
				anchors.verticalCenter: parent.verticalCenter

				Text {
					text: model.title
					width: parent.width
					font: UiConstants.TitleFont
					wrapMode: Text.Wrap
				}
			}

			CountBubble {
				id: topicRepliesCount
				anchors.right: topicItemImage.left
				anchors.verticalCenter: parent.verticalCenter
				value: model.numReplies
				visible: value > 0
			}

			Image {
				id: topicItemImage
				source: "image://theme/icon-m-common-drilldown-arrow" + (theme.inverted ? "-inverse" : "")
				anchors.right: parent.right
				anchors.verticalCenter: parent.verticalCenter
			}

			onClicked: {
				pageStack.push(Qt.resolvedUrl("TopicPage.qml"), {
				                      board: forumPage.board,
				                      topicId: model.topicId
								  });
			}
		}
	}

	ScrollDecorator {
		flickableItem: topicsView
	}

	BusyIndicator {
		anchors.centerIn: parent
		platformStyle: BusyIndicatorStyle { size: "large" }
		visible: topicsView.count == 0 && board.busy
		running: visible
	}
}
