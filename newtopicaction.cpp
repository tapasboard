#include <QtCore/QDebug>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>

#include "global.h"
#include "board.h"
#include "xmlrpcinterface.h"
#include "xmlrpcreply.h"
#include "newtopicaction.h"
#include "fetchtopicsaction.h"

NewTopicAction::NewTopicAction(int forumId, const QString &subject, const QString &text, Board *board)
	: Action(board), _forumId(forumId), _subject(subject), _text(text)
{
}

bool NewTopicAction::isSupersetOf(Action *action) const
{
	Q_UNUSED(action);
	return false;
}

void NewTopicAction::execute()
{
	_call = _board->service()->asyncCall("new_topic",
	                                     QString::number(_forumId),
	                                     _subject.toUtf8(),
	                                     _text.toUtf8()
	                                     );
	_call->setParent(this);
	connect(_call, SIGNAL(finished(XmlRpcPendingCall*)), SLOT(handleFinishedCall()));
}

void NewTopicAction::handleFinishedCall()
{
	XmlRpcReply<QVariantMap> result(_call);
	if (result.isValid()) {
		QVariantMap map = result;
		bool post_ok = map["result"].toBool();
		if (post_ok) {
			int state = map["state"].toInt();
			if (state == 1) {
				// Awaiting moderation
				// TODO
			} else {
				// Refresh topics
				_board->enqueueAction(new FetchTopicsAction(_forumId,
				                                            Board::Normal,
				                                            0,
				                                            FORUM_PAGE_SIZE,
				                                            _board));
			}
		} else {
			qWarning() << "Could not submit topic:" << map["result_text"].toString();
		}
	} else {
		qWarning() << "Could not submit topic";
		// TODO emit error ...
	}
	emit finished(this);
	_call->deleteLater();
}
