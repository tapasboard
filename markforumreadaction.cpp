#include <QtCore/QDebug>

#include "board.h"
#include "xmlrpcinterface.h"
#include "xmlrpcreply.h"
#include "fetchforumsaction.h"
#include "markforumreadaction.h"

MarkForumReadAction::MarkForumReadAction(int forumId, Board *board) :
    Action(board), _forumId(forumId)
{
}

bool MarkForumReadAction::isSupersetOf(Action *action) const
{
	MarkForumReadAction *other = qobject_cast<MarkForumReadAction*>(action);
	if (other) {
		return _forumId == other->_forumId;
	}
	return false;
}

void MarkForumReadAction::execute()
{
	if (_forumId == 0) {
		_call = _board->service()->asyncCall("mark_all_as_read");
	} else {
		_call = _board->service()->asyncCall("mark_all_as_read", QString(_forumId));
	}
	_call->setParent(this);
	connect(_call, SIGNAL(finished(XmlRpcPendingCall*)), SLOT(handleFinishedCall()));
}

void MarkForumReadAction::handleFinishedCall()
{
	XmlRpcReply<QVariantMap> result(_call);
	if (result.isValid()) {
		QVariantMap map = result;
		bool result_ok = map["result"].toBool();
		if (result_ok) {
			// Must reload forums to see the result!
			_board->enqueueAction(new FetchForumsAction(_board));
		} else {
			qWarning() << "Failed to mark forums: " << map["result_text"].toString();
		}
	} else {
		qWarning() << "Could not fetch board configuration";
		// TODO emit error ...
	}
	emit finished(this);
	_call->deleteLater();
}
