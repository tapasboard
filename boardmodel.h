#ifndef BOARDMODEL_H
#define BOARDMODEL_H

#include <QtCore/QAbstractListModel>
#include <QtSql/QSqlQuery>

class Board;

class BoardModel : public QAbstractListModel
{
	Q_OBJECT
	Q_PROPERTY(Board * board READ board WRITE setBoard NOTIFY boardChanged)
	Q_PROPERTY(int forumId READ forumId WRITE setForumId NOTIFY forumIdChanged)

public:
	BoardModel(QObject *parent = 0);

	enum DataRoles {
		NameRole = Qt::DisplayRole,
		LogoRole = Qt::DecorationRole,
		DescriptionRole = Qt::StatusTipRole,

		ForumIdRole = Qt::UserRole,
		SubOnlyRole,
		CategoryRole,
		UnreadRole
	};

	Board * board() const;
	void setBoard(Board *board);

	int forumId() const;
	void setForumId(const int id);

	int rowCount(const QModelIndex &parent = QModelIndex()) const;
	QVariant data(const QModelIndex &index, int role) const;

	bool canFetchMore(const QModelIndex &parent = QModelIndex()) const;
	void fetchMore(const QModelIndex &parent = QModelIndex());

public slots:
	void refresh();
	void markSubforumsRead();

signals:
	void boardChanged();
	void forumIdChanged();

private slots:
	void reload();
	void handleForumChanged(int forumId);

private:
	Board *_board;
	int _forumId;

	mutable QSqlQuery _query;
	int _records;
	bool _eof;
};

#endif // BOARDMODEL_H
