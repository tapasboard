#ifndef GLOBAL_H
#define GLOBAL_H

#include "boardmanager.h"

/** Time the forum config settings should be considered up to date, in days. */
#define BOARD_CONFIG_TTL  2

/** Time the list of forums should be considered up to date, in days. */
#define BOARD_LIST_TTL  2

/** Time we should consider the most recent topics in a forum up to date, in seconds. */
#define FORUM_TOP_TLL 15 * 60

/** Time we should consider other topics in a forum up to date, in seconds. */
#define FORUM_TOPICS_TLL 15 * 60

/** Time we should keep topics in the cache, in days. */
#define FORUM_TOPICS_CACHE 10

/** Number of topics per "block" in subforum view */
#define FORUM_PAGE_SIZE 20

/** This is the absolute maximum page size the API will allow. */
#define MAX_FORUM_PAGE_SIZE 50

/** Time we should consider the most recent posts in a topic up to date, in seconds. */
#define TOPIC_TOP_TLL 5 * 60

/** Time we should consider other posts in a topic up to date, in seconds. */
#define TOPIC_POSTS_TLL 15 * 60

/** Time we should keep topics in the cache, in days. */
#define TOPIC_POSTS_CACHE 10

/** Number of posts per "block" in topic view */
#define TOPIC_PAGE_SIZE 20

/** This is the absolute maximum page size the API will allow. */
#define MAX_TOPIC_PAGE_SIZE 50

// Some singletons
extern BoardManager *board_manager;

#endif // GLOBAL_H
